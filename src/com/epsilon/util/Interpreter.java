package com.epsilon.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

//*************************************************
// INTERPRETER CLASS
//*************************************************

public class Interpreter {
	

	public static void main(String args[]) throws IOException, FileNotFoundException{
    }

//*************************************************
// INTERPRETER FUNCTION
//*************************************************
	
    public static String interpHTMLString(String MessageString, 
    									String MessageSourceIP, 
    									String PathXML,
										Boolean CreateTGIn)
    {
    	String tempString;
   	
    	//BEFORE WE GO TO FAR LETS MAKE SURE WE BELONG HERE
    	//IF THIS IS XML OR HTML DATA JUST RETURN IT AND DON'T PREPROCESS
    	
  	  	if (MessageString.startsWith("<?xml") || MessageString.startsWith("<!-- HTML")) return MessageString;
 
 		tempString = MessageString;
 		
 		if (CreateTGIn) 
 		try
		{
			//WRITE FILE 
			FileWriter fstream = new FileWriter(PathXML+"TGInput.xml");
			BufferedWriter out = new BufferedWriter(fstream);
			out.write(buildTicketGenieInput(MessageString));
			//CLOSE THE OUTPUT STREAM
			out.close();
		}
		catch (Exception e)
		{//CATCH ANY EXCEPTIONS
			  System.err.println("Error: " + e.getMessage());
		}
 		
  		return tempString + "\n<interpreted>";
  	}
    
  //*************************************************
 // RESPONSE FUNCTION
 //*************************************************
 	
     public static String responseString(String MessageString, 
     									String MessageSourceIP, 
     									String PathXML)
     {
     	String tempString;
     	
  		tempString = "PING RESPONSE TEST";
  		
   		return tempString;
   	}
     
   

//*************************************************
// BUILD TICKET GENIE INPUT FILE
//*************************************************
    
    private static String buildTicketGenieInput(String InputTicket)
    {
    
		String tempString;
		tempString = "<ticket><line index=\"1\" text=\"\" length=\"0\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"2\" text=\"         375\" length=\"40\" fieldmask=\"1111111112222222222222222222222222222222\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"3\" text=\"\" length=\"0\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"4\" text=\"Table:\" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"5\" text=\"Customer:\" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"6\" text=\"Phone:\" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"7\" text=\"09/27/12                          02:12pm\" length=\"40\" fieldmask=\"11111111111111111111111111111111112222222\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"8\" text=\"\" length=\"0\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"9\" text=\"[Seat 1]\" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"10\" text=\"Lox                 \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"11\" text=\"Prime Rib Au Jus    \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"12\" text=\" Rare               \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"13\" text=\" w/Saut Onion       \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"14\" text=\" **Mashed Potato**  \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"15\" text=\" Plain              \" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"16\" text=\"\" length=\"0\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"><line index=\"17\" text=\"Server:Bob G\" length=\"40\" fieldmask=\"1111111111111111111111111111111111111111\" filtermask=\"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\" type=\"unknown\" format=\"unknown\" lead=\"\" trail=\"<br>\"></ticket><formats><format name=\"Header Bold\" type= \"head\" lead=\"<h1><b>\" trail=\"</b></h1>\" color=\"Blue\"> <format name=\"Header Norm\" type= \"head\" lead=\"<h2><b>\" trail=\"</b></h2>\" color=\"Blue\"> <format name=\"Header Fine\" type= \"head\" lead=\"<h3><b>\" trail=\"</b></h3>\" color=\"Blue\"> <format name=\"Item Norm\" type= \"item\" lead=\"<h2><b>\" trail=\"</b></h2>\" color=\"Black\"> <format name=\"Item Fine\" type= \"item\" lead=\"<h3><b>\" trail=\"</b></h3>\" color=\"Black\"> <format name=\"Modifier Norm\" type= \"mod\" lead=\"<h2><b>\" trail=\"</b></h2>\" color=\"Red\"> <format name=\"Modifier Fine\" type= \"mod\" lead=\"<h3><b>\" trail=\"</b></h3>\" color=\"Red\"><format name=\"Footer Bold\" type= \"foot\" lead=\"<h1><b>\" trail=\"</b></h1>\" color=\"Green\"> <format name=\"Footer Norm\" type= \"foot\" lead=\"<h2><b>\" trail=\"</b></h2>\" color=\"Green\"></formats><colors><color name=\"Red\" lead=\"<font color='red'>\" trail=\"</font>\"> <color name=\"Black\" lead=\"<font color='black'>\" trail=\"</font>\"> <color name=\"Blue\" lead=\"<font color='blue'>\" trail=\"</font>\"> <color name=\"Green\" lead=\"<font color='green'>\" trail=\"</font>\"></colors>";
		return tempString;
		    	
    }
}
